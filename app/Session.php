<?php

/**
 * Class Session
 */
class Session
{

    /**
     * @var Session
     */
    static private $_instance;

    /**
     * @return Session
     */
    static public function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = new Session();
        }
        return self::$_instance;
    }

    /**
     * @param string $key
     * @param mixed $value
     * Crée une cellule dans la variable _SESSION
     */
    public function setValue($key, $value){
        global $_SESSION;
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * Retourne l'information demandée
     * @return null|string
     */
    public function getValue($key){
        global $_SESSION;
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }
        return null;
    }

}

?>