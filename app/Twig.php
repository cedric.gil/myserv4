<?php

/**
 * Class Twig
 */
class Twig
{

    /**
     * @var Twig
     */
    private static $_instance;

    /**
     * @var \Twig\Environment
     */
    private $twig;

    /**
     * @return Twig
     */
    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = New Twig();
        }
        return self::$_instance;
    }

    /**
     * Twig constructor.
     */
    private function __construct(){
        $loader = new Twig\Loader\FilesystemLoader('../pages');
        $this->twig = new Twig\Environment($loader, [
            'cache' => false
        ]);
    }

    /**
     * @param string $page
     * @param string $spage
     * @param array $paramsfromphpfile
     * Retourne la page demandée
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getRender($page, $spage, $paramsfromphpfile){
        $params = $paramsfromphpfile;
        switch ($page){
            case 'home':
                return $this->twig->render('home.twig', $params);
            case 'auth':
                return $this->twig->render('auth.twig', $params);
            case 'my':
                switch ($spage){
                    case 'home':
                        return $this->twig->render('my/home.twig', $params);
                    case 'welcome':
                        return $this->twig->render('my/welcome.twig', $params);
                    default:
                        return '';

                }
            default:
                return $this->twig->render('404.twig');
        }
    }

    /**
     * @param string $page
     * Retourne le chemin du fichier PHP lié à la page
     * @return bool|string
     */
    public function getPhpPath($page, $spage){
        switch ($page){
            case 'auth':
                return '../pages/php/auth.php';
            case 'my':
                switch ($spage) {
                    case 'home':
                        return '../pages/my/php/home.php';
                    case 'welcome':
                        return false;
                    case 'create':
                        return '../pages/my/php/create.php';
                    default:
                        break;
                }
            default:
                break;
        }
    }

}