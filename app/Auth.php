<?php

/**
 * Class Auth
 */
class Auth
{

    /**
     * Singleton
     * @var Auth
     */
    private static $_instance;

    /**
     * @var Database
     */
    private $db;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Database $db
     * @param Config $config
     * @return Auth
     */
    public static function getInstance($db){
        if(is_null(self::$_instance)){
            self::$_instance = new Auth($db);
        }
        return self::$_instance;
    }

    /**
     * Auth constructor.
     * @param Database $db
     * @param Config $config
     */
    private function __construct($db){
        $this->db = $db;
        $this->config = $db->getConfig();
    }

    /**
     * @param string $login
     * @param string $pass
     * @param string $email
     * @param string $passc
     * Vérifie si l'utilisateur peut être créé, puis le crée
     * @return string
     */
    public function register($login, $email, $pass, $passc){
        if($pass === $passc){
            if($login != $pass){
                if(strlen($pass) >= 8 && preg_match('/[A-Z]/', $pass) && preg_match('/[0-9]/', $pass)){
                    $pass = hash('SHA512', $pass.$this->config->get('gds'));
                    $this->db->checkInjection([$login, $email, $pass]);
                    if($this->exist(['user' => $login, 'email' => $email]) === false){
                        $this->db->insert("INSERT INTO users (user, email, password) VALUES (?, ?, ?)", [$login, $email, $pass]);
                        return 'ok';
                    }
                    else{
                        return 'L\'identifiant ou l\'email saisi est déjà utilisé';
                    }
                }
                else{
                    return 'Le mot de passe saisi n\'est pas assez robuste';
                }
            }
            else{
                return 'L\'identifiant ne peut pas être identique au mot de passe';
            }
        }
        else{
            return 'Les mots de passe ne sont pas identiques';
        }
    }

    /**
     * @param array $array
     * Vérifie si une information existe déjà
     * @return bool
     */
    private function exist($array){
        $query = "SELECT * FROM users WHERE";
        foreach($array as $key => $value){
            if(isset($or)){
                $query = $query." OR";
            }
            $query .= " $key = '$value'";
            $or = true;
        }
        if(sizeof($this->db->select($query)) > 0){
            return true;
        }
        return false;
    }

    /**
     * @param string $login
     * @param string $pass
     * Vérifie si les informations d'authentification correspondent à un utilisateur
     * @return string
     */
    public function login($login, $pass){
        $pass = hash('SHA512', $pass.$this->config->get('gds'));
        $this->db->checkInjection([$login, $pass]);
        $query = "SELECT user, password FROM users WHERE user = '$login' AND password = '$pass'";
        if(sizeof($this->db->select($query)) > 0){
            return 'ok';
        }
        else{
            return 'Identifiant ou mot de passe incorrect';
        }
    }

}

?>