<?php

class Server
{

    /**
     * Singleton
     * @var Server
     */
    private static $_instance;

    /**
     * @var Database
     */
    private $db;

    /**
     * @var \Thedudeguy\Rcon
     */
    private $rcon;

    /**
     * @var \Thedudeguy\Query
     */
    private $query;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $state;

    /**
     * @param $db Database
     * @param $rcon \Thedudeguy\Rcon
     * @param $query \Thedudeguy\Query
     */
    public static function getInstance($db, $rcon, $query, $user){
        if(is_null(self::$_instance)){
            self::$_instance = new Server($db, $rcon, $query, $user);
        }
        return self::$_instance;
    }

    /**
     * Server constructor.
     * @param $db Database
     * @param $rcon \Thedudeguy\Rcon
     * @param $query \Thedudeguy\Query
     */
    private function __construct($db, $rcon, $query, $user){
        $this->db = $db;
        $this->rcon = $rcon;
        $this->query = $query;
        $this->user = $user;
    }

    /**
     * Crée un nouveau serveur
     * @param $version string
     * @param $welcome_msg string
     * @param $nb_player string
     * @param $gamemode string
     * @param $difficulty string
     * @param $onlinemode string
     */
    public function create($version, $welcome_msg, $nb_player, $gamemode, $difficulty, $onlinemode){
        // Création du serveur dans la BDD
        $query = 'SELECT id FROM users WHERE user = \''.$this->user.'\'';
        $id = $this->db->select($query)[0]['id'];
        $query = 'SELECT portquery, portrcon FROM servers ORDER BY portquery DESC';
        $ports = $this->db->select($query)[0];
        $portquery = $ports['portquery']+1;
        $portrcon = $ports['portrcon']+1;
        $rconpwd = $this->getRandString();
        $query = 'INSERT INTO servers (idusers, portquery, portrcon, rconpwd) VALUES (?, ?, ?, ?)';
        $this->db->insert($query, [$id, $portquery, $portrcon, $rconpwd]);

        // Création du fichier de pré-configuration
        $file = fopen('E:\myserv\temp\config.txt', 'w+');
        fwrite($file, $version);
        fwrite($file, "\n".$welcome_msg);
        fwrite($file, "\n".$nb_player);
        fwrite($file, "\n".$gamemode);
        fwrite($file, "\n".$difficulty);
        fwrite($file, "\n".$onlinemode);
        fwrite($file, "\n".$this->user);
        fwrite($file, "\n".$portquery);
        fwrite($file, "\n".$portrcon);
        fwrite($file, "\n".$rconpwd);
        fclose($file);

        // Appel du scripts PowerShell
        $cmd = 'powershell E:\myserv\create.ps1';
        system(escapeshellcmd($cmd));

        $this->state = 'create';
    }

    /**
     * Génère une chaîne de caractère aléatoire
     * @param int $length
     * @return string
     */
    private function getRandString($length=20){
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';
        for($i=0; $i<$length; $i++){
            $string .= $chars[rand(0, strlen($chars)-1)];
        }
        return $string;
    }

    /**
     * Retourne l'état actuel du serveur
     * @return string
     */
    public function getState(){
        if($this->superviseState() != 'miss_fold'){
            $ok = false;
            while($ok == false){
                if($this->superviseState() != 'empty_logs'){
                    $ok = true;
                }
            }
            return $this->state;
        }
        return 'empty';
    }

    /**
     * Vérifie l'état du serveur et retourne la véracité de celui-ci
     * @return string
     */
    private function superviseState(){
        $dir = 'E:\myserv\servers\\'.$this->user;
        $path = $dir.'\logs\latest.log';
        // Vérification de l'existance des logs
        if(is_dir($dir)){
            if(file_exists($path) == true){
                $file = fopen($path, 'a+');
                if(filesize($path) < 1){
                    $size = 1;
                }
                else {
                    $size = filesize($path);
                }

                // Lecture des logs
                $logs = fread($file, $size);
                if(str_contains($logs, 'STARTEDNOW')){
                    $this->state = 'starting';
                }
                elseif(str_contains($logs, 'RCON Listener stopped')){
                    $file2 = fopen('E:\myserv\temp\config.txt', 'w+');
                    fwrite($file2, $this->user);
                    fclose($file2);
                    $this->state = 'starting';
                    fwrite($file, 'STARTEDNOW');
                    fclose($file);
                    $cmd = 'powershell E:\myserv\start.ps1';
                    system(escapeshellcmd($cmd));
                }
                elseif(str_contains($logs, 'RCON Listener started')){
                    $this->state = 'running';
                }
                else {
                    $this->state = 'starting';
                    $this->startednow = false;
                }
                return 'ok';
            }
            return 'empty_logs';
        }
        return 'miss_fold';
    }

    public function sendCmd($cmd){
        if($this->rcon->connect()){
            $this->rcon->sendCommand($cmd);
        }
        else{
            echo "ah";
        }
    }

}

?>