<?php

/**
 * Class Engine
 */
class Engine
{

    /**
     * @param string $page
     * @param string $spage
     * @param array $paramsfromphpfile
     * Retourne le rendu Twig
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public static function TwigRender($page, $spage, $paramsfromphpfile){
        return Twig::getInstance()->getRender($page, $spage, $paramsfromphpfile);
    }

    /**
     * @param string $page
     * Retourne le chemin du fichier PHP lié à la page
     * @return bool|string
     */
    public static function getPhpPath($page, $spage){
        return Twig::getInstance()->getPhpPath($page, $spage);
    }

    /**
     * Enregiste un nouvel utilisateur
     * @param array $post
     * @return string
     */
    public static function AuthRegister($post){
        $post['user'] = strtolower($post['user']);
        $post['email'] = strtolower($post['email']);
        $res = Auth::getInstance(Database::getInstance(Config::getInstance()))->register($post['user'], $post['email'], $post['password'], $post['passwordc']);
        if ($res === 'ok') {
            Session::getInstance()->setValue('user', $post['user']);
            echo '<script type="text/javascript">document.location = "?p=my"</script>';
        }
        else {
            echo '<script type="text/javascript">window.alert("'.$res.'")</script>';
        }
        return $res;
    }

    /**
     * Authentifie un nouvel utilisateur
     * @param array $post
     * @return string
     */
    public static function AuthLogin($post){
        $post['user'] = strtolower($post['user']);
        $res =  Auth::getInstance(Database::getInstance(Config::getInstance()))->login($post['user'], $post['password']);
        if ($res === 'ok') {
            Session::getInstance()->setValue('user', $post['user']);
            echo '<script type="text/javascript">document.location = "?p=my"</script>';
        }
        else {
            echo '<script type="text/javascript">window.alert("'.$res.'")</script>';
        }
        return $res;
    }

    /**
     * Retourne la valeur demandée du tableau $_SESSION
     * @param string $key
     * @return null|string
     */
    public static function getSessionValue($key){
        return Session::getInstance()->getValue($key);
    }

    /**
     * Vérifie si l'utilisateur est nouveau
     * @return bool
     */
    public static function UserNew(){
        $db = Database::getInstance(Config::getInstance());
        $user = Session::getInstance()->getValue('user');
        $query = 'SELECT new FROM users WHERE user = \''.$user.'\'';
        $res = $db->select($query);
        if (sizeof($res) > 0) {
            if ($res[0]['new'] == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Enlève le statut 'nouveau' de l'utilisateur
     */
    public static function updateUserNew(){
        $db = Database::getInstance(Config::getInstance());
        $user = Session::getInstance()->getValue('user');
        $query = 'UPDATE users SET new = \'0\' WHERE user = \''.$user.'\'';
        $db->insert($query);
    }

    private static function getInstanceServer(){
        $user = Session::getInstance()->getValue('user');
        $db = Database::getInstance(Config::getInstance());
        $query = 'SELECT portrcon, portquery, rconpwd FROM servers WHERE idusers = (SELECT id FROM users WHERE user = \''.$user.'\')';
        $res = $db->select($query)[0];
        $rcon = \Thedudeguy\Rcon::getInstance('192.168.1.2', $res['portrcon'], $res['rconpwd'], 3);
        $query = \Thedudeguy\Query::getInstance('192.168.1.2', $res['portquery']);
        return Server::getInstance(Database::getInstance(Config::getInstance()), $rcon, $query, $user);
    }

    /**
     * Crée un nouveau serveur
     * @param $post array
     */
    public static function ServerCreate($post){
        self::getInstanceServer()->create($post['version'], $post['welcome_msg'], $post['nb_player'], $post['gamemode'], $post['difficulty'], $post['onlinemode']);
    }

    /**
     * Retourne l'état du serveur
     * @return string
     */
    public static function ServerGetState() {
        if(Session::getInstance()->getValue('user') != null){
            return self::getInstanceServer()->getState();
        }
    }

    public static function sendCmd($cmd) {
        self::getInstanceServer()->sendCmd($cmd);
    }

}

?>