var choixLogin = document.getElementById('choix_login')
var choixRegister = document.getElementById('choix_register')
var tbodyLogin = document.getElementById('login')
var tbodyRegister = document.getElementById('register')
var divAuth = document.getElementById('auth')

window.onload = init()

function init () {
    setTimeout(divDisp, 100)
}
function divDisp () {
    divAuth.style.opacity = '100%'
}

choixLogin.addEventListener('click', dispLogin)
choixRegister.addEventListener('click', dispRegister)

function dispLogin () {
    tbodyRegister.style.display = 'none'
    tbodyLogin.style.display = 'table-row-group'
    choixRegister.style.fontSize = '1.3em'
    choixRegister.style.color = '#d2d2d2'
    choixLogin.style.fontSize = '1.5em'
    choixLogin.style.color = 'white'
    divAuth.style.height = '270px'
}
function dispRegister () {
    choixLogin.style.fontSize = '1.3em'
    choixLogin.style.color = '#d2d2d2'
    choixRegister.style.fontSize = '1.5em'
    choixRegister.style.color = 'white'
    divAuth.style.height = '365px'
    setTimeout(dispRegister2, 75)
}
function dispRegister2 () {
    tbodyLogin.style.display = 'none'
    tbodyRegister.style.display = 'table-row-group'
}