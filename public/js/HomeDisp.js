var logoHome = document.getElementById('logo')
var pHome = document.getElementsByTagName('p')
var buttonHome = document.getElementsByClassName('button')

window.onload = init

function init(){
    setTimeout(logoDisp, 200)
    setTimeout(pDisp, 1200)
    setTimeout(buttonDisp, 1200)
}
function logoDisp() {
    logoHome.style.opacity = '100%'

}
function pDisp() {
    for (let i = 0; i < pHome.length; i++) {
        pHome[i].style.opacity = '100%'
    }

}
function buttonDisp() {
    for (let i = 0; i < buttonHome.length; i++) {
        buttonHome[i].style.opacity = '100%'
    }
}