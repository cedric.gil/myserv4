var containerNewUser = document.getElementById('newuser')
var buttonNext = document.getElementById('next')
var buttonBack = document.getElementById('back')
var current = 1

window.onload = init()

function init(){
    setTimeout(containerDisp, 500)
}
function containerDisp(){
    containerNewUser.style.opacity = '100%'
    document.getElementById(current.toString()).style.display = 'inline'
    document.getElementById(current.toString()).style.opacity = '100%'
}

buttonNext.addEventListener('click', Next)
buttonBack.addEventListener('click', Back)

function Next(){
    if(current < 8){
        document.getElementById(current.toString()).style.opacity = '0%'
        let next = current + 1
        document.getElementById(next.toString()).style.display = 'inline'
        setTimeout(function (){
            document.getElementById(current.toString()).style.display = 'none'
            document.getElementById(next.toString()).style.opacity = '100%'
            if(current == 6){
                buttonNext.innerHTML = '<span>Terminer</span><img src="img/next.png">'
            }
            if(current == 7){
                buttonBack.style.opacity = '0%';
                buttonNext.setAttribute('type', 'submit')
                buttonNext.setAttribute('form', 'formwelcome')
                buttonNext.setAttribute('name', 'create')
                buttonNext.innerHTML = '<span>Créer</span><img src="img/next.png">'
            }
            current = next
        }, 500)
    }
}
function Back(){
    if(current > 1){
        document.getElementById(current.toString()).style.opacity = '0%'
        let back = current - 1
        document.getElementById(back.toString()).style.display = 'inline'
        setTimeout(function (){
            document.getElementById(current.toString()).style.display = 'none'
            document.getElementById(back.toString()).style.opacity = '100%'
            current = back
        }, 500)
    }
}